const courseData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing, elit. Impedit dolorem esse cumque dolore asperiores quas, maiores explicabo quasi, exercitationem laudantium, deserunt qui? Fugiat dicta porro dolor ducimus facilis tempora et.",
		price: "45000",
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing, elit. Impedit dolorem esse cumque dolore asperiores quas, maiores explicabo quasi, exercitationem laudantium, deserunt qui? Fugiat dicta porro dolor ducimus facilis tempora et.",
		price: "50000",
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing, elit. Impedit dolorem esse cumque dolore asperiores quas, maiores explicabo quasi, exercitationem laudantium, deserunt qui? Fugiat dicta porro dolor ducimus facilis tempora et.",
		price: "55000",
		onOffer: true
	}
]

export default courseData;