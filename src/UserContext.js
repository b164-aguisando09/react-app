import React from 'react';


//creates a context object.
//use to store information that can be shared to other components

const UserContext = React.createContext();

//provider component allows other components to consume/use the context object and supply the necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;