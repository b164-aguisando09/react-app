import {Container, Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>Learn From Home</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Libero, dolorum illum in, distinctio excepturi voluptas maxime necessitatibus, ex, magnam impedit vero optio. Rerum, obcaecati. Incidunt eveniet a exercitationem eum quas.
							</Card.Text>
						</Card.Body>
				 </Card>
			</Col>
			<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>Study Now, Pay Later</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Libero, dolorum illum in, distinctio excepturi voluptas maxime necessitatibus, ex, magnam impedit vero optio. Rerum, obcaecati. Incidunt eveniet a exercitationem eum quas.
							</Card.Text>
						</Card.Body>
					</Card>
			</Col>
			<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>Be Part of Our Community</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Libero, dolorum illum in, distinctio excepturi voluptas maxime necessitatibus, ex, magnam impedit vero optio. Rerum, obcaecati. Incidunt eveniet a exercitationem eum quas.
							</Card.Text>
						</Card.Body>
				</Card>
			</Col>
		</Row>
		)
}