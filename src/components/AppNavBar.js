
import {Container, Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react';
import UserContext from '../UserContext'



export default function AppNavBar(){

	/*
		Syntax:
		 localStorage.getItem(propertyName)
	*//*
	const [user, setUser] = useState(localStorage.getItem("email"));*/

	const {user} = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand as={Link} to="/" >Bookly</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  	<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto" variant="tabs">
					<Nav.Item>
						<Link className="nav-link" to='/'>Home</Link>
					</Nav.Item>
					<Nav.Item>
						<Link className="nav-link" to='/courses'>Courses</Link>
					</Nav.Item>
					{	
						(user.id !== null) ?
							<Nav.Item>
								<Link className="nav-link" to='/logout'>Logout</Link>
							</Nav.Item>
						:
						<Fragment>
							<Nav.Item>
								<Link className="nav-link" to='/register'>Register</Link>
							</Nav.Item>
							<Nav.Item>
								<Link className="nav-link" to='/login'>Login</Link>
							</Nav.Item>
						</Fragment>
					}
				
				
				</Nav>
			</Navbar.Collapse>
		  </Container>
		</Navbar>
		)
}