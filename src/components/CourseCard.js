import {Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';



export default function CourseCard({courseProp}){

	//use the state hook for this component to be able to store its state

	/*

		Syntax:
			const [getter, setter] = useState(initialGetterValue)

	*/

	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	const [button, setButton] = useState(false);

/*	function enroll(){
		if (seat > 0) {
			setCount(count + 1);
			setSeat(seat - 1);
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'No more seats.',
				text: 'Please enroll on another schedule. Thank You!'
			});
			setButton(true);
		}
	}*/

/*	function enroll(){
			setCount(count + 1);
			setSeat(seat - 1);
		}

	useEffect(() => {
		if(seat <= 0){
			Swal.fire({
				icon: 'warning',
				title: 'No more seats.',
				text: 'Please enroll on another schedule. Thank You!'
			});
		}
	}, [seat])*/

	const {name, description, price, _id} = courseProp

	return(
		<Card className = "m-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count} Enrollees</Card.Text>
				<Card.Subtitle>Seats:</Card.Subtitle>
				<Card.Text>{seat} Seats left</Card.Text>
				<Button variant="primary" as={Link} to={`/courses/${_id}`} disabled={button}>Details</Button>
			</Card.Body>
		</Card>
		)
}