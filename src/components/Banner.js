import {Row, Col, Button} from 'react-bootstrap';

export default function Banner(){
	return (
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>A place to learn coding skills</p>
				<Button>Enroll Now</Button>
			</Col>	
		</Row>
		)
}