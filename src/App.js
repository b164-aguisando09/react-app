import {useState, useEffect} from 'react';
import './App.css';
import AppNavBar from './components/AppNavBar';
import CourseCard from './components/CourseCard';

import Home from './pages/Home';
import Course from './pages/Course';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/login';
import Error from './pages/Error';
import Logout from './pages/Logout';

import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext'

//utilities from react router dom library
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
//BrowserRouter => as parent component
/*
   Routes - group all the designated route for the app. a new component introduced in V6 of react router dom. whose task is to allow switching between locations. old syntax (switch);
   

*/
/*
   Route element - Route path='' element={}
   path = is used to assign a URI endpoint
   element = identify which element/component to be rendered
*/

function App() {

   const [user, setUser] = useState({
      id: null,
      isAdmin: null
   })

   //function for clearing localStorage on logout
   const unsetUser = () => {
      localStorage.clear();
   }

   useEffect(() => {
      fetch('http://localhost:4000/api/users/details', {
         headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
         }
      })
      .then(res => res.json())
      .then(data => {
         if(typeof data._id !== "undefined"){
            setUser({
               id: data._id,
               isAdmin: data.isAdmin
            })
         }else{
            setUser({
               id: null,
               isAdmin: null
            })
         }
      })
   }, [])

  return (

//React context is nothing but a global state to the app. it is a way to make particular data available to all the components no matter how they are nested.
   <UserProvider value = {{user, setUser, unsetUser}}>
    <Router >
       <AppNavBar/>
       <Container className="my-5 mx-auto">
       <Routes>
          <Route path= '/' element={<Home/>}/>
          <Route path= '/login' element={<Login/>} />
          <Route path= '/register' element={<Register/>}/>
          <Route path= '/courses' element={<Course/>}/>
          <Route path= '/courses/:courseId' element={<CourseView/>}/>
          <Route path= '/logout' element={<Logout/>}/>
          <Route path= '*' element={<Error/>}/>
       </Routes>
       </Container>
    </Router>
   </UserProvider>
  );
}

export default App;
