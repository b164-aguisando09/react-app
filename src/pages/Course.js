//import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';
import {Fragment, useEffect, useState} from 'react';

export default function Courses(){

	const [courses, setCourses] = useState([])
	
/*	const courses = courseData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course} />
			)
	})*/

	useEffect(() => {
		fetch('http://localhost:4000/api/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
					)
		}))
		})
	}, [])

	return(
		<Fragment>
			<h1>Courses</h1>
		{courses}
		</Fragment>
		
		)
}