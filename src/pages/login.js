import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
const acc = {
	email: "admin@mail.com",
	password: "admin"
};

export default function Login(){

	//userContext hook is used to deconstruct the data of the UserContext
	  const {user, setUser} = useContext(UserContext);
	  const [email, setEmail] = useState("");
	  const [password, setPassword] = useState("");
	  const [isActive, setIsActive] = useState(false);
	  const [alertBtn, setAlertBtn] = useState(false);

	  function LoginUser(e) {

	  	/*
				Syntax
					fetch("URL", {options})
					.then(res => res.json)
					.then(data => {})
	 	*/

	  	//prevents page redirection via a form submission
	 	e.preventDefault();


	 	//Clear input fields
	 	setEmail("");
	 	setPassword("");

	  	fetch('http://localhost:4000/api/users/login', {
	  			method: 'POST',
	  			headers: {
	  				'Content-Type': 'application/json'
	  			},
	  			body: JSON.stringify({
	  				email: email,
	  				password: password
	  			})
	  		})
	  		.then(res => res.json())
	  		.then(data => {
	  			console.log(data)
	  			if(typeof data.accessToken !== "undefined"){
	  				localStorage.setItem('token', data.accessToken)
	  				retrieveUserDetails(data.accessToken)

	  				Swal.fire({
	  				      icon: 'success',
	  				      title: 'Login Successfully.',
	  				      text: 'Welcome to Bookly!'
	  				    })
	  				.then((result) => {
	  					setAlertBtn(true);
	  				})
	  			}else{

	  				Swal.fire({
	  				      icon: 'warning',
	  				      title: 'Login Failed',
	  				      text: 'Incorrect password or email address!'
	  				    });
	  			}
	  		})

	  		/*
				Syntax:
					localStorage.setItem(Key, Value)
	  		*/
	  		//localStorage.setItem("email", email);

	  		//set the global state to have properties from local storage
	  		/*setUser({
	  			email: localStorage.getItem('email')
	  		})*/

	  		const retrieveUserDetails = (token) => {
	  			fetch('http://localhost:4000/api/users/details', {
	  				headers: {
	  					Authorization: `Bearer ${token}`
	  				}
	  			})
	  			.then(res => res.json())
	  			.then(data => {
	  				console.log(data)
	  				setUser({
	  					id: data._id,
	  					isAdmin: data.isAdmin
	  				})
	  			})
	  		}


	}

	 useEffect(() => {
	  		    if (email !== "" && password !== "") {
	  		      setIsActive(true);
	  		    }else{
	  		      setIsActive(false);
	  		    }
	  		  }, [email, password])


	return(

		(alertBtn && user.id !== null) ?

			<Navigate to="/courses"/>

		:

			<Form onSubmit= {(e) => LoginUser(e)}>
			<h1 className="text-center">Login</h1>
			  <Form.Group 
			    className="mb-3" 
			    controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			      type="email" 
			      placeholder="Enter email"
			      value={email}
			    /*target.value => the value inside the input field. when onChange is called*/
			      onChange={e => setEmail(e.target.value)}
			      required
			       />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    type="password" 
			    placeholder="Password" 
			    value={password}
			    onChange={e => setPassword(e.target.value)}
			    required />
			  </Form.Group>
			  {
			    isActive ?
			      <Button variant="primary" type="submit" id="submitBtn">
			    Login
			  </Button>
			  :
			    <Button variant="secondary" type="submit" id="submitBtn" disabled>
			    Login
			  </Button>
			  }
			
			</Form>
		)
}