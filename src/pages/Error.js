import {Container} from 'react-bootstrap';

import Banner from './../components/Banner';

export default function ErrorPage(){
	return(
		<Container>
			<h1 className = "text-center my-3 ">404 Page Not Found</h1>
		</Container>
		);
};